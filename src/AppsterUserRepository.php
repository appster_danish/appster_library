<?php

namespace Appster\Basic;


use Illuminate\Support\Facades\Config;
use Appster\Basic\Contracts\UserContract as AppsterUserContract;

class AppsterUserRepository implements AppsterUserInterface
{
  
    public function find()
    {
        $userModels = Config::get('services.appster.userModel');

        if (! is_array($userModels)) {
            $userModels = [$userModels];
        }

        foreach ($userModels as $userModel) {
           return $model = $this->createUserModel($userModel);
        }

    }
    
    
     protected function createUserModel($class)
    {
        $model = new $class;

        if (! $model instanceof AppsterUserContract) {
            throw new \InvalidArgumentException('Model does not implement User.');
        }

        return $model;
    }
    






    
}
