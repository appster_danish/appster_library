<?php

namespace Appster\Basic;

use Illuminate\Support\Facades\Config;
use DB;
use Appster\Basic\Utility\ResponseFormatter;
use Mail;
//use App\Helpers\Utility\ImageHelper;

trait UserBasic {

    protected $userDeviceModel;
    protected $response;

    /**
     * Write the entity to persistent storage.
     *
     * @return void
     */
    public function saveUserInstance() {
        $this->save();
    }

    /**
     * 
     * @param function : getUserDeviceModel()
     * @returns Description : Get Device Model.
     * 
     */
    public function getUserDeviceModel() {
        $userModels = Config::get('services.appster.userDeviceModel');
        if (strlen($userModels) > 0) {
            return new $userModels;
        }
    }

    /**
     * 
     * @param function : signUp()
     * @returns Description : Here we are registering user.
     * 
     */
    public function signUp($json) {
        $response = new ResponseFormatter();
        try {
            DB::beginTransaction();
            if (isset($json['referral_code']) && $json['referral_code'] != '') {
                $referred_by = User::referredIdByCode($json['referral_code']);
                if ($referred_by == '') {
                    return $this->response->responseServerError(trans('appster::messages.incorrect_email'));
                }
            }
            // Add User Information
            $user = $this->AddUser($json);
            $deviceInfo = $json['deviceInfo'];
            // Store Device Information
            $token = $this->addDevice($user->id, $deviceInfo);
            $user->accessToken = $token;
            $isVarificaitonMailEnable = $userModels = Config::get('services.appster.isVarificaitonMailEnable');
            if ($isVarificaitonMailEnable) {
                $this->sendEmailVerificationLink($user);
            }
            //$user->profile_pic = ImageHelper::getImages($user->id, Config('constant.UserProfilePic'), $user->profile_pic);
            $user->user_id = $user->id;
            $user->role_id = 0;
            $user->isTempPass = 0;
            $user->facebook_id = '';
            unset($user->id);
            unset($user->role_id);
            unset($user->verification_code);
            unset($user->remember_token);
            unset($user->referral_code);
            unset($user->referred_by);
            unset($user->referred_count);
            DB::commit();
            return $response->responseSuccess(trans('appster::messages.user_register_success'), $user);
        } catch (Exception $ex) {
            DB::rollBack();
            return $response->responseServerError($ex->getMessage());
        }
    }

    /**
     * 
     * @param function : addDevice()
     * @returns Description : insert device in device table.
     * 
     */
    public function addDevice($user_id, $deviceInfo) {
        $userDeviceModel = $this->getUserDeviceModel();
        if (isset($user_id)) {
            $userDeviceModel->where('user_id', $user_id)->delete();
        }
        // if found deviceToken then delete.    
        if (isset($deviceInfo['deviceToken'])) {
            $userDeviceModel->where('device_token', $deviceInfo['deviceToken'])->delete();
        }
        // if found uniqueDeviceId then delete.  
        if (isset($deviceInfo['deviceId'])) {
            $userDeviceModel->where('unique_device_id', $deviceInfo['deviceId'])->delete();
        }

        $userDeviceModel = $this->getUserDeviceModel();
        $userDeviceModel->user_id = $user_id;
        if (isset($deviceInfo['deviceId'])) {
            $userDeviceModel->unique_device_id = $deviceInfo['deviceId'];
        }
        if (isset($deviceInfo['deviceType'])) {
            $userDeviceModel->device_type = $deviceInfo['deviceType'];
        }
        if (isset($deviceInfo['deviceToken'])) {
            $userDeviceModel->device_token = $deviceInfo['deviceToken'];
        }
        $userDeviceModel->save();
        $userDeviceModel->access_token = md5($userDeviceModel->id . $userDeviceModel->user_id . time());
        $userDeviceModel->save();
        return $userDeviceModel->access_token;
    }

    /**
     * 
     * @param function : AddUser()
     * @returns Description : insert user .
     * 
     */
    public function AddUser($data) {

        $isVarificaitonMailEnable = $userModels = Config::get('services.appster.isVarificaitonMailEnable');
        $status = 1;
        if ($isVarificaitonMailEnable) {
            $status = 0;
        }
        $verificationCode = time();
        $this->name = $data['fullname'];
        $this->email = $data['email'];
        $this->password = bcrypt($data['password']);
        $this->role_id = 2;
        $this->phone_number = isset($data['phoneNumber']) ? $data['phoneNumber'] : '';
        $this->status = $status;
        $this->verification_code = $verificationCode;
        $this->saveUserInstance();
        return $this;
    }

    /**
     * 
     * @param function : login()
     * @returns Description : Here we are authenticating user.
     * 
     */
    public function login($json) {
        try {

            $this->response = new ResponseFormatter();
            $deviceInfo = $json['deviceInfo'];
            if (\Auth::attempt(['email' => $json['email'], 'password' => $json['password']])) {
                $user = $this->find(\Auth::user()->id);
                if (is_object($user)) {
                    $isVarificaitonMailEnable = $userModels = Config::get('services.appster.isVarificaitonMailEnable');
                    if ($isVarificaitonMailEnable) {

                        if ($user->status == 0) {
                            $this->sendEmailVerificationLink($user);
                            return $this->response->responseUnauthorized(trans('appster::messages.EmailVerifyError'));
                        }
                    }
                    if ($user->status == 2) {
                        return $this->response->responseUnauthorized(trans('appster::messages.userBlocked'));
                    }
                    $user->last_login = date('Y-m-d h:i:s');
                    $user->save();
                    $token = $this->addDevice($user->id, $deviceInfo);
                    $user->accessToken = $token;
                    //$user->profile_pic = ImageHelper::getImages($user->id, config('constant.PROFILE_IMAGE'), $user->profile_pic);
                    $user->user_id = $user->id;
                    unset($user->id);
                    unset($user->role_id);
                    unset($user->verification_code);
                    unset($user->remember_token);
                    unset($user->referral_code);
                    unset($user->referred_by);
                    unset($user->referred_count);
                }
                return $this->response->responseSuccess(trans('appster::messages.login_success'), $user);
            } else {
                $userDetail = $this->findUserByEmail($json['email']);
                if (!is_object($userDetail)) {
                    return $this->response->responseUnauthorized(trans('appster::messages.userNotRegistered'));
                }
                return $this->response->responseUnauthorized(trans('appster::messages.credential_mismatched'));
            }
        } catch (\Exception $ex) {
            return $this->response->responseServerError($ex->getMessage());
        }
    }

    /**
     * find user by emailID
     */
    public function findUserByEmail($email) {
        return $this->select('name', 'id', 'role_id', 'is_profile_finished', 'email', 'status')->where('email', $email)->first();
    }

    /**
     * 
     * @param function : ChangePassword()
     * @returns Description : Here we are updating password of the user.
     * 
     */
    public function ChangePassword($userId, $oldPassword, $newPassword) {

        try {
            $this->response = new ResponseFormatter();
            $checkPassword = $this->where('id', $userId)
                    ->where('status', 1)
                    ->select('*')
                    ->first();

            if (is_object($checkPassword)) {
                if (!\Hash::check($oldPassword, $checkPassword->password)) {
                    return $this->response->responseUnauthorized(trans('appster::messages.OldPasswordWrong'));
                }
                $updateRespnse = $this->updatePassword($userId, $newPassword);
                if ($updateRespnse) {
                    return $this->response->responseSuccess(trans('appster::messages.success'));
                }
            }
            return $this->response->responseUnauthorized(trans('appster::messages.unauthorized'));
        } catch (\Exception $ex) {

            return $this->response->responseServerError($ex->getMessage());
        }
    }

    /**
     * 
     * @param function : updatePassword()
     * @returns Description : Here we are updating password.
     * 
     */
    public function updatePassword($userId, $newPassword) {

        $user = $this->find($userId);
        $user->password = bcrypt($newPassword);
        $user->is_temp_pass = 0;
        $user->save();

        return $user;
    }

    /**
     * 
     * @param function : forgotPassword()
     * @returns Description : Here we are sending duplicate password of the user.
     * 
     */
    public function forgotPassword($email) {
        try {
            $this->response = new ResponseFormatter();
            $tempPassword = \App\Helpers\Utility\UtilityHelper::generateRandomString(10);
            $userInfo = $this->where('email', $email)
                    ->select('*')
                    ->first();

            if (!$userInfo) {
                return $this->response->responseUnauthorized(trans('appster::messages.user_not_found_reset_pass'));
            }

            if ($userInfo['status'] == 0) {
                return $this->response->responseUnauthorized(trans('appster::messages.userNotVerified'));
            }

            $updateRespnse = $this->updatePassword($userInfo["id"], $tempPassword);
            $userData = array('email' => $email, 'name' => $userInfo['name']);
            Mail::send('appsterView::email.resetpassword', ['tempPassword' => $tempPassword, 'name' => $userInfo['name'], 'email' => $email], function($message) use ($userData) {
                $message->to($userData['email'], $userData['name'])->subject('Hyve : Temporary Password Request');
            });
            return $this->response->responseSuccess(trans('appster::messages.mail_sent'));
        } catch (\Exception $ex) {
            return $this->response->responseServerError($ex->getMessage());
        }
    }

    /**
     * 
     * @param function : socialLogin()
     * @returns Description : Here we are validating facebook user.
     * 
     */
    public function socialLogin($json) {
        try {
            $this->response = new ResponseFormatter();
            $client = new \GuzzleHttp\Client();
            $facebookToken = $json['facebookToken'];

            $res = $client->get('https://graph.facebook.com/v2.8/me?fields=name,picture,email&access_token=' . $facebookToken);

            if ($res->getStatusCode() != 200) {
                $status = 'Login Invalid';
                return $this->response->responseServerError(trans('appster::messages.facebook_login_fail'));
            }
            
            
            $facebookUser = json_decode($res->getBody());
            if ($facebookUser->id != $json['facebookId']) {
                return $this->response->responseServerError(trans('appster::messages.facebook_mismatch'), '11');
            }
            
            $email = $json['email'];
            $facebookId = $json['facebookId'];
            $name = $json['fullname'];
            $userData = array("email" => $email, "facebookId" => $facebookId, "name" => $name);
            $socialuser = $this->socialSignIn($userData);
            $deviceInfo = $json['deviceInfo'];
            // Store Device Information                
            $token = $this->addDevice($socialuser->id, $deviceInfo);
            $socialuser->accessToken = $token;
         //   $socialuser->profile_pic = ImageHelper::getImages($socialuser->id, 1, $socialuser->profile_pic);
            $socialuser->user_id = $socialuser->id;
            unset($socialuser->id);
            return $this->response->responseSuccess(trans('appster::messages.facebook_login_success'));
        } catch (\Exception $ex) {
            return $this->response->responseServerError($ex->getMessage());
        }
    }

    /**
     * 
     * @param function : socialSignIn()
     * @returns Description : Here we are authenticating user after validating facebook user.
     * 
     */
    public function socialSignIn($userData) {
        $user = $this->where('facebook_id', $userData['facebookId'])
                        ->orWhere('email', $userData['email'])->first();
        if (is_object($user)) {
            $user->facebook_id = $userData['facebookId'];
            $user->name = $userData['name'];
            $user->save();
        } else {
            $referralCode = \App\Helpers\Utility\UtilityHelper::generateRandomString(8);
            $verificationCode = time();

            $this->facebook_id = $userData['facebookId'];
            $this->email = $userData['email'];
            $this->name = $userData['name'];
            $this->role_id = 2;
            $this->phone_number = isset($userData['phoneNumber']) ? $userData['phoneNumber'] : '';
            $this->verification_code = $verificationCode;
            $this->referral_code = $referralCode;
            $this->status = 1;
            $this->saveUserInstance();
            $this->socialSignIn($userData);
        }
        return $user;
    }

    /**
     * 
     * @param function : logout()
     * @returns Description : Here we are throwing user from the system.
     * 
     */
    public function logout($userId, $accessToken) {
        try {
            $this->response = new ResponseFormatter();
            $user = $this->unRegister_all($accessToken);
            if ($user) {
                return $this->response->responseSuccess(trans('appster::messages.logout'));
            } else {
                return $this->response->responseUnauthorized(trans('appster::messages.unauthorized'));
            }
        } catch (\Exception $ex) {
            return $this->response->responseServerError($ex->getMessage());
        }
    }

    /**
     * 
     * @param function : unRegister_all()
     * @returns Description : Here we are fetching user .
     * 
     */
    public function unRegister_all($accessToken) {

        $userDeviceModel = $this->getUserDeviceModel();
        $userDeviceModel->where('access_token', $accessToken)->delete();
        return true;
    }

    /**
     * 
     * @param function : sendEmailVerificationLink()
     * @returns Description : Here we are sending verification link to the user.
     * 
     */
    public function sendEmailVerificationLink($userData) {
        $verificationlink = base64_encode($userData->verification_code);
        Mail::send('appsterView::email.verificationemail', ['name' => $userData->name, 'url' => $verificationlink, 'email' => $userData->email], function($message) use ($userData) {
            $message->to($userData->email, $userData->name)->subject('Email Verification Link');
        });
    }

    public function varification($confirmation_code) {
        $user = $this::select('id')->where('verification_code', base64_decode($confirmation_code))->first();
        if (!$user) {
            $message = "Invitation Already Accepted";
            $data = ['message' => $message];
        } else {
            $user->status = 1;
            $user->verification_code = '';
            $user->save();
            $message = "Invitation Accepted";
            $data = ['message' => $message];
        }
        return view('appsterView::verificationPage', $data);
    }

}
