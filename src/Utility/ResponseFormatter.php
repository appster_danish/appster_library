<?php

namespace Appster\Basic\Utility;

use Log;
use \Symfony\Component\HttpFoundation\Response;

class ResponseFormatter {

    public function responseSuccess($message = '', $data = array()) {
        $response = array(
            'status' => 1,
            'status_code' => Response::HTTP_OK,
            'message' => $message,
            'result' => (object) $data,
        );
        return $response;
    }

    public function responseCustom($statusCode, $message = '', $data = array()) {
        $response = array(
            'status' => 0,
            'status_code' => $statusCode,
            'message' => $message,
            'result' => (object) $data,
        );
        return $response;
    }

    public function responseUnauthorized($message = '', $data = array()) {
        $response = array(
            'status' => 0,
            'status_code' => Response::HTTP_UNAUTHORIZED,
            'message' => $message,
            'result' => (object) $data,
        );
        return $response;
    }

    public function responseBadRequest($message = '', $data = array()) {

        $response = array(
            'status' => 0,
            'status_code' => Response::HTTP_BAD_REQUEST,
            'message' => $message,
            'result' => (object) $data,
        );
        return $response;
    }

    public function responseNotFound($message = '', $data = array()) {
        $response = array(
            'status' => 0,
            'status_code' => Response::HTTP_NOT_FOUND,
            'message' => $message,
            'result' => (object) $data,
        );
        Log::error($response);
        return $response;
    }

    public function responseServerError($message = '', $data = array()) {
        $response = array(
            'status' => 0,
            'status_code' => Response::HTTP_INTERNAL_SERVER_ERROR,
            'message' => $message,
            'result' => (object) $data,
        );
        
        Log::error($response);
        
        return $response;
    }

}
