# Appster Basic

Appster Basic is a library for basic functionality like SignUp,login,forget, and change password.

## How does it work?

This package expects that you are using Laravel 5.3 or above.
You will need to import the `appster/basic` package via composer:

```shell
composer require appster/basic
```

### Configuration

Add the service provider to your `config/app.php` file within the `providers` key:

```php
// ...
'providers' => [
    /*
     * Package Service Providers...
     */

    Appster\Basic\UserBasicServiceProvider::class,
],
// ...
```
### Configuration for local environment only

If you wish to enable generators only for your local environment, you should install it via composer using the --dev option like this:

```shell
composer require appster/basic --dev
```

## Database

Attach on git file 


## Example App

here is a link for example app
